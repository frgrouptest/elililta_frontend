import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myListFilterPipe',
  pure: false
})
export class MyListFilterPipePipe implements PipeTransform {

    transform(items: any[], filter: any): any {
        if (!items || !filter) {
            return items;
        }
        // filter items array, items which match and return true will be
        // kept, false will be filtered out
        return items.filter(item => ( (item.Name.toLowerCase().indexOf(filter.toLowerCase()) !== -1) || (item.date_in.indexOf(filter) !== -1) ) );
    }

}
