import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { StockService } from "../../pages/default/inner/_services/stock.service";


declare let mLayout: any;
@Component({
    selector: "app-header-nav",
    templateUrl: "./header-nav.component.html",
    styleUrls: ["./header-nav.component.css"],
    encapsulation: ViewEncapsulation.None,
})
export class HeaderNavComponent implements OnInit, AfterViewInit {

    public notfications : any = [];

    constructor(private stockService : StockService) {

    }
    ngOnInit() {

        this.stockService.getItemsLowInStock().subscribe(res => {
            this.notfications = [];
            for(let i=0; i < res.length; i++) {
                this.notfications.push({message : ""+ res[i].name + " low in stock !"});
            }
        },errRes => {

        });

    }
    ngAfterViewInit() {

        mLayout.initHeader();

    }

    updateNotfication(){
        this.stockService.getItemsLowInStock().subscribe(res => {
            this.notfications = [];
            for(let i=0; i < res.length; i++) {
                this.notfications.push({message : ""+ res[i].name + " low in stock !"});
            }
        },errRes => {

        });
    }
}