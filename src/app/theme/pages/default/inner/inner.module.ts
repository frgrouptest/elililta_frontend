import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { MatTableModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { Daterangepicker } from 'ng2-daterangepicker';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from "@angular/common";
import { DecimalPipe } from '@angular/common';
import { BusyModule } from 'angular2-busy';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

// ~ my custom Services import
import { ItemService } from './_services/item.service';
import { CustomerService } from "./_services/customer.service";
import { BillingService } from "./_services/billing.service";
import { StockService } from "./_services/stock.service";
import { ProjectService } from "./_services/project.service";
//
import { MyListFilterPipePipe } from "../../../../pipes/my-list-filter-pipe.pipe";

import { InnerComponent } from './inner.component';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from '../default.component';
import { CustomersComponent } from './customers/customers.component';
import { ItemsComponent } from './items/items.component';
import { ProformaComponent } from './proforma/proforma.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SalesComponent } from './sales/sales.component';
import { ProjectAllocationComponent } from './project-allocation/project-allocation.component';
import { NewProformaComponent } from './new-proforma/new-proforma.component';
import { NewSaleComponent } from "./new-sale/new-sale.component";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "redirectTo": "dashboard",
                "pathMatch": "full"
            },
            {
                "path": "items",
                "component": ItemsComponent
            },
            {
                "path": "customers",
                "component": CustomersComponent
            },
            {
                "path": "proformas",
                "component": ProformaComponent
            },
            {
                "path": "newProforma",
                "component": NewProformaComponent
            },{
                "path": "newSale",
                "component": NewSaleComponent
            },
            {
                "path": "dashboard",
                "component": DashboardComponent
            },
            {
                "path": "sales",
                "component": SalesComponent
            },
            {
                "path": "project_allocation",
                "component": ProjectAllocationComponent
            }

        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule,
        MatTableModule, MatFormFieldModule, MatInputModule, InfiniteScrollModule,
        FormsModule , ReactiveFormsModule, NgbModule.forRoot(), Daterangepicker , ChartsModule, BusyModule
    ], exports: [
        RouterModule
    ], declarations: [
        InnerComponent,
        CustomersComponent,
        ItemsComponent,
        ProformaComponent,
        DashboardComponent,
        SalesComponent,
        ProjectAllocationComponent,
        NewProformaComponent,
        NewSaleComponent,
        MyListFilterPipePipe
    ],
    providers: [ItemService, CustomerService, ProjectService, BillingService, StockService, DatePipe, DecimalPipe ]
})
export class InnerModule {



}