import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';
import {Restangular} from "ngx-restangular";

@Injectable()
export class ItemService {

    private itemsBaseURL;
    private itemCategoriesBaseURL;
    private itemSubCategoriesBaseURL;
    private itemPricesBaseURL;
    constructor(private restangular: Restangular) {
        // setting api call end-points
      this.itemsBaseURL = this.restangular.all('Items');
      this.itemCategoriesBaseURL = this.restangular.all('categories');
      this.itemSubCategoriesBaseURL = this.restangular.all('sub_Categories');
      this.itemPricesBaseURL = this.restangular.all('item_Price');
    }


    getAllItems() :any {
    return this.itemsBaseURL.getList();
    }

    addItem(itemData: any) :any {
      return this.itemsBaseURL.post(itemData);
    }

    getSubCategoriesOfCategory( _categoryId : number){
        return this.itemSubCategoriesBaseURL.all('find').getList({categoryId: _categoryId});
    }

    updateItem(itemId: number, itemData){
        return this.itemsBaseURL.customPUT(itemData, ''+ itemId );
    }

    findItemLike(query: any) :any {
        return this.itemsBaseURL.customPOST(query, 'findLike');
    }

    // this method can be used to send delete request. Not dependent on routing
    delete(Item: any) :any {
        return Item.remove();
    }

    // ~ Item categories ~ //
    getAllItemCategories() :any {
        return this.itemCategoriesBaseURL.getList();
    }

    addItemCategory(itemCategory: any) :any {
        return this.itemCategoriesBaseURL.post(itemCategory);
    }
    

    addItemSubCategory(itemCategory: any) :any {
        return this.itemSubCategoriesBaseURL.post(itemCategory);
    }

    // ~ register item prices ~ //
    addItemPrice(itemPrice: any) :any {
        return this.itemPricesBaseURL.post(itemPrice);
    }

    findItem(query) : any {
        return this.itemsBaseURL.customPOST(query,'find');
    }

    findItemPrices(itemId : any) :any {
        return this.itemPricesBaseURL.customPOST( { item_id: itemId}, 'find');
    }

}
