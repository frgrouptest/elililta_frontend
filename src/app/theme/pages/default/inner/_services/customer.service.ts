import { Injectable } from '@angular/core';

import {Restangular} from "ngx-restangular";

@Injectable()
export class CustomerService {

    private customerBaseURL;

    constructor(private restangular: Restangular){
        this.customerBaseURL = this.restangular.all('customers');
    }

    getAllCustomers() :any {
        return this.customerBaseURL.getList();
    }

    findCustomer(query: any) :any {
        return this.customerBaseURL.customPOST(query, 'find');
    }

    addCustomer(newCustomer: any) :any {
        return this.customerBaseURL.post(newCustomer);
    }

    // this method can be used to send delete request. Not dependent on routing
    static delete(customer: any) :any {
        return customer.remove();
    }

}