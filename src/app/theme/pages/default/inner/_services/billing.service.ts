// ~ comment about this service ~ //

// ~ Load Module Dependencies ~ //
import { Injectable } from '@angular/core';
import {Restangular} from "ngx-restangular";

import { BillItem } from "../_models/BillItems.model";

@Injectable()
export class BillingService {

    private proformaBaseURL;
    private salesBaseURL;
    private stockItemsBaseURL;
    private proformaItemsBaseURL;
    private saleItemsBaseURL;

    constructor(private restangular: Restangular){
        this.proformaBaseURL = this.restangular.all('proforma');
        this.salesBaseURL = this.restangular.all('sales');
        this.stockItemsBaseURL = this.restangular.all('stock_Items');
        this.proformaItemsBaseURL = this.restangular.all('proforma_Items');
        this.saleItemsBaseURL = this.restangular.all('sale_Items');
    }


    saveProforma(proformaDetails: any, billItems : BillItem[] ){
         return this.proformaBaseURL.customPOST({
           proformaDetail : proformaDetails,
           proformaBillItems : billItems
        }, 'saveProforma');
    }

    // itemPrice1 is used to decrease the stock value -- since it is based on item price 1
    saveSale(saleDetails: any, billItems : BillItem[]){
        return this.stockItemsBaseURL.customPOST({
            saleDetail : saleDetails,
            saleBillItems : billItems
        }, 'saveSale');
    }

    getAllProformas(_offset,_limit){
        return this.proformaBaseURL.getList(
            {
                offset:_offset,
                limit:_limit
            });
    }

    searchProformaWithKeyword(keyword_ : string){
        return this.proformaBaseURL.all('proformaWithKeyword').getList({keyword: keyword_});
    }

    searchSaleWithKeyword(keyword_ : string){
        return this.salesBaseURL.all('saleWithKeyword').getList({keyword: keyword_});
    }

    getAllSales(_offset,_limit) {
        return this.salesBaseURL.getList(
            {
                offset:_offset,
                limit:_limit
            });
    }

    searchProformaByDateRange(_start_ : Date, _end_ : Date) {
        return this.proformaBaseURL.customPOST({
            start : _start_,
            end : _end_
        },'findByDateRange');
    }

    searchSaleByDateRange(_start_ : Date, _end_ : Date) {
        return this.salesBaseURL.customPOST({
            start : _start_,
            end : _end_
        },'findByDateRange');
    }

    // get monthly report of sales
    getMonthlyReportSales(_offset, _limit){
        return this.salesBaseURL.get('monthlyReportSales', {
            offset:_offset,
            limit:_limit
        });
    }

    // get weekly report of sales
    getWeeklyReportSales(_offset, _limit){
        return this.salesBaseURL.get('weeklyReportSales', {
            offset:_offset,
            limit:_limit
        });
    }

    // get daily report of sales
    getDailyReportSales(_offset, _limit){
        return this.salesBaseURL.get('dailyReportSales' ,{
            offset:_offset,
            limit:_limit
        });
    }

    // get monthly report of sale items wise
    findMonthlySoldItemsReport(month_number, year){
        return this.salesBaseURL.get('findMonthlySoldItemsReport' ,{
            month_number:month_number,
            year:year
        });
    }

    getTodaysSale() {
        return this.salesBaseURL.get('getTodaysSale');
    }

    // ~ fetches items of proforma using id ~ //
    getItemOfProformaBill(id){
        return this.proformaItemsBaseURL.post({proforma_ID: id});
    }

    // ~ fetches items of proforma using id ~ //
    getItemOfSaleBill(id){
        return this.saleItemsBaseURL.post({sales_ID: id});
    }

    static delete(profroma: any) :any {
        return profroma.remove();
    }

    getcurrentFSNO(){
        return this.salesBaseURL.get('getcurrentFSNO');
    }

}