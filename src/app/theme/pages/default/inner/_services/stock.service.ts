// ~ A service to fetch stock informations ~ //

// ~ Load Module Dependencies ~ //
import { Injectable } from '@angular/core';
import {Restangular} from "ngx-restangular";


@Injectable()
export class StockService {

    private stockBaseURL;
    private stockItemsBaseURL;

    constructor(private restangular: Restangular){
        this.stockBaseURL = this.restangular.all('stocks');
        this.stockItemsBaseURL = this.restangular.all('stock_Items');
    }

    // Add Items to stock
    stockIn(stockInData) : any {
        return this.stockItemsBaseURL.post(stockInData);
    }

    checkQuantityAvailable(_itemId: number, _quantity: number){
        let body = {itemId : _itemId, quantity: _quantity};
        return this.stockItemsBaseURL.customPOST(body, 'checkItemQuantityAvailability');
    }

    // get stock information with stock id
    getStockDetails() : any  {
        return this.stockBaseURL.get('getStockDetails');
    }

    // get stock information with stock id
    getItemsLowInStock() : any  {
        return this.stockBaseURL.get('getItemsLowInStock');
    }

    // category wise report of the stock
    getStockValueWithCategory(){
        return this.stockItemsBaseURL.get('byCategory');
    }

    // product wise of current stock
    getStockValueWithProduct(){
        return this.stockItemsBaseURL.get('byProduct');
    }

    // fetch stock in history log
    getStockInHistory(){
        return this.stockItemsBaseURL.get('stockIns');
    }

}