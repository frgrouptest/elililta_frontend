import { Injectable } from '@angular/core';

import {Restangular} from "ngx-restangular";

@Injectable()
export class ProjectService {

    private projectBaseURL;

    constructor(private restangular: Restangular){
        this.projectBaseURL = this.restangular.all('project');
    }

    getAllProjects() :any {
        return this.projectBaseURL.getList();
    }

    findProjectItems(query: any) :any {
        return this.projectBaseURL.customPOST(query, 'findItemsOfProject');
    }

    addProject(newProject: any) :any {
        return this.projectBaseURL.post(newProject);
    }

    updateProjectAllocatedItems(newAllocatedItems: any) :any {
        return this.projectBaseURL.customPOST(newAllocatedItems, 'updateAllocatedItems');
    }

    // this method can be used to send delete request. Not dependent on routing
    static delete(project: any) :any {
        return project.remove();
    }

}