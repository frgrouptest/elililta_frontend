import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MatTableDataSource} from '@angular/material';

import { ItemService } from "../_services/item.service";
import {BillItem} from "../_models/BillItems.model";
import {StockService} from "../_services/stock.service";

import { Subscription } from 'rxjs';
import {ProjectService} from "../_services/project.service";

declare let toastr: any;
declare let swal: any;
declare var $ : any;

@Component({
  selector: 'app-project-allocation',
  templateUrl: "project-allocation.component.html",
  styleUrls: ['./project-allocation.component.css']
})
export class ProjectAllocationComponent implements OnInit {

    // Projects
    projects : any[] = [];

    totalAllocatedItemPriceValue : number = 0;

    // ~ item list for select table view ~ //
    selectedRowIndex: number = -1;
    itemDisplayedColumns = [ 'Custom-ID','Name', 'Stock Price' ];
    public dataSource;

    busy: Subscription;

    selectedItemName : string;
    selectedItemId: number;
    selectedItemCustomId: string;
    selectedItemQuantity: number = 0;
    selectedItemPriceValue: number = 0;

    // ~ items for proforma ~ //
    projectAllocationItems : BillItem[] = [];

    items : any [] = [];
    newProjectForm : FormGroup;

    constructor(private fb: FormBuilder, private projectService : ProjectService,
                private itemService: ItemService, private stockService: StockService) {
        this.newProjectForm = fb.group({
            'name' : [null, Validators.required],
            'amount' : [null, Validators.required],
            'client' : [null, Validators.required],
            'agreement' : [null],
            'address' : [null],
            'projectItems': [null]
        });


        // ~ Populating items table data
        itemService.getAllItems().subscribe( items_ => {
            this.items = items_;
            this.dataSource = new MatTableDataSource(this.items);
            this.dataSource.filterPredicate = function customFilter(data , filter:string ): boolean {
                let tmp = data.name;
                tmp = tmp.toLowerCase();
                return (tmp.indexOf(filter) == 0);
            }

        }, errRes => {
            toastr.error(errRes.data[0].msg);
        });

    }

    ngOnInit() {
        // toastr warning
        this.busy = this.projectService.getAllProjects().subscribe( res => {
            this.projects = res;
        }, err => {
            toastr.error('Error while loading projects !');
        });
    }

    // Item selected form list of items for proforma list
    itemRowSelected(row){
        this.selectedRowIndex = row.id;
        this.selectedItemCustomId = row.custom_id;
        this.selectedItemId = row.id;
        this.selectedItemName = row.name;
        this.selectedItemPriceValue = row.stock_price;
    }


    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    }


    addtoAllocatedItems(){
        if(this.selectedRowIndex == -1 || this.selectedItemName == null){
            toastr.warning('Select Item first');
            return;
        }

        this.busy = this.stockService.checkQuantityAvailable(this.selectedItemId, this.selectedItemQuantity).subscribe(res => {

            if(res.message != "Continue"){
                swal("Stop !", res.message , "warning");
            }

            if(res.message == "Continue") {
                let newProjectItem = new BillItem();
                newProjectItem.name = this.selectedItemName;
                newProjectItem.item_price = this.selectedItemPriceValue;
                newProjectItem.itemId = this.selectedItemId;
                newProjectItem.quantity = this.selectedItemQuantity;
                newProjectItem.custom_id = this.selectedItemCustomId;

                if (this.selectedItemQuantity <= 0 || this.selectedItemPriceValue <= 0) {
                    toastr.warning("Fill out required fields !");
                    return;
                }


                for (let i = 0; i < this.projectAllocationItems.length; i++) {
                    if (this.projectAllocationItems[i].custom_id == this.selectedItemCustomId) {
                        this.projectAllocationItems[i].quantity += this.selectedItemQuantity;

                        // update total price of allocated items value
                        this.updateTotalValue();

                        return;
                    }
                }

                this.projectAllocationItems.push(newProjectItem);

                // update total price of allocated items value
                this.updateTotalValue();

                // ~ resetting proforma item details ~ //
                this.selectedItemName = '';
                this.selectedItemCustomId = '';
                this.selectedItemId = null;
                this.selectedRowIndex = -1;
                this.selectedItemPriceValue = null;
                this.selectedItemQuantity = null;
            }


        });


    }


    registerProject(formData: any){

        formData.projectItems = this.projectAllocationItems;

        if(this.projectAllocationItems.length == 0){
            toastr.warning('No Items allocated for this project');
            return;
        }

        this.projectService.addProject(formData).subscribe( res => {
            toastr.success('Project added');
            this.updateProjectList();
            this.newProjectForm.reset();
        }, err => {
            if(err.status === 400 && err.data.length > 0 ){
                err.data.forEach( function (val) {
                    toastr.error("" + val.msg);
                });
                return;
            }
            toastr.error('Something went wrong !');
        });

    }

    deleteProject(project : any){
        console.log(project);


        swal({
            title: project.name +" is about to be removed !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    if(project != null){
                        ProjectService.delete(project).subscribe(res => {
                            toastr.success("Project " + project.name + " is deleted !");
                            this.updateProjectList();
                        }, err => {
                            swal(err.data['message'], {
                                icon: "error",
                            });
                        });
                    }
                }
            });

    }

    updateProjectList(){
        this.projects = [];
        this.busy = this.projectService.getAllProjects().subscribe( res => {
            this.projects = res;
        }, err => {
            toastr.error('Error while loading projects !');
        });
    }

    showItemsAllocation(project : any){
        this.projectAllocationItems = [];
        this.totalAllocatedItemPriceValue = 0;
        this.projectService.findProjectItems({project_Id: project.id}).subscribe( res=> {
            console.log(res);
            this.projectAllocationItems = res.map( item => {
                this.totalAllocatedItemPriceValue += (item.item_price * item.quantity);
                return item;
            });


            $('#m_modal_List').modal('show');

        }, err => {
            toastr.error('Error while fetching items of project ');
        });

    }

    // This method is called when a new project button is clicked so as to clear previously loaded project Items
    resetProjectAllocationItems(){
        this.projectAllocationItems = [];
    }

    itemQuantityChanged(event){
        let target = event.target || event.srcElement || event.currentTarget;
        let item_id = target.id;
        console.log(target);
        console.log(target.value);

        // check Item availability

        this.projectAllocationItems = this.projectAllocationItems.map( item => {
            if(item.id  == item_id){
                item.quantity = target.value;
            }
            return item;
        });
        this.updateTotalValue();
    }

    updateTotalValue(){
        this.totalAllocatedItemPriceValue = 0;
        this.projectAllocationItems.map( item => {
            this.totalAllocatedItemPriceValue += (item.item_price * item.quantity);
        });
    }

    updateAllocatedProjectItems(){

        if(this.projectAllocationItems.length == 0){
            toastr.error('Can not Update ! \n No Items allocated for this project');
            return;
        }

        this.projectService.updateProjectAllocatedItems(this.projectAllocationItems).subscribe( res => {
            toastr.success('Project Updated');
            this.updateProjectList();
            this.projectAllocationItems = [];
            this.totalAllocatedItemPriceValue = 0;
        }, err => {
            if(err.status === 400 && err.data.length > 0 ){
                err.data.forEach( function (val) {
                    toastr.error("" + val.msg);
                });
                return;
            }
            toastr.error('Something went wrong !');
        });
    }

}
