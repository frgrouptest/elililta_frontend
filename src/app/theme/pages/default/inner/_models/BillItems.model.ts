// ~ Class of bill items on proforma or sale ~ //
export class BillItem {
    id: number;
    name: string;
    subcategory: string;
    itemId: number;
    kg_per_bar: number;
    custom_id: string;
    quantity: number;
    item_price: number;
    item_originalPrice : number; // weak implementation I know
    unit: string;
}