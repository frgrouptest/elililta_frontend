export interface Customer {
    id: number;
    name: string;
    TIN_NO: string;
    phone: string;
    address: string;
}