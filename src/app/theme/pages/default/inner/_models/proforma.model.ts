export interface Proforma {
    id: number;
    bill_to: string;
    TIN_NO: number;
    address: string;
    proforma_date: string;
    reference: string;
    FSNO: number;
    discount: number;
    amount: number;
}