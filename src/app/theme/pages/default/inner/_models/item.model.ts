export interface Item {
    id: number;
    custom_id: string;
    name: string;
    color: string;
    unit: string;
    kg_per_bar : number;
    stock_price: number;
    category: string;
    subcategory: string;
    item_description: string;
    stock_reorder_level: number;
}